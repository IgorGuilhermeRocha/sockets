package com.redes.projeto;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private ServerSocket serverSocket;

    public Server(int port) throws Exception {
        super();
        this.serverSocket = new ServerSocket(port);

    }

    public static void main(String[] args) throws Exception {
        Server server = new Server(15500);
        Socket clientSocket = null;

        while (true) {
            try {
                clientSocket = server.serverSocket.accept();
                new Thread(new Protocol(clientSocket)).start();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Cant initiate server");
            }
        }
    }

}
