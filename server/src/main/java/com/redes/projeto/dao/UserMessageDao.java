package com.redes.projeto.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.redes.projeto.entities.Message;
import com.redes.projeto.entities.User;
import com.redes.projeto.entities.UserMessage;

public class UserMessageDao {

    private Connection conn;
    private String sql;

    public void InsertUserMessage(int messageId, int senderId, int recipientId) {
        this.sql = "INSERT INTO USER_MESSAGE VALUES(default,? ,? ,?, ?)";

        try {
            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, senderId);
            ps.setInt(2, recipientId);
            Date date = new Date(System.currentTimeMillis());
            ps.setDate(3, date);
            ps.setInt(4, messageId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }
    }

    public List<UserMessage> selectAllMessagesByUserId(int userId) {

        this.sql = "SELECT M.title, M.description, UM.user_message_id, UM.fk_sender, UM.fk_recipient, UM.date_message FROM USER_MESSAGE AS UM INNER JOIN MESSAGE AS M ON UM.fk_message = M.message_id WHERE UM.fk_sender =? or UM.fk_recipient =?";

        List<UserMessage> listUserMessage = new ArrayList<>();
        UserMessage userMessage = new UserMessage();
        Message message = new Message();
        User user = new User();

        try {

            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, userId);
            ps.setInt(2, userId);

            ResultSet rs = ps.executeQuery();
            boolean test = false;

            while (rs.next()) {

                userMessage.setUserMessageId(rs.getInt("UM.user_message_id"));
                message.setTitle(rs.getString("M.title"));
                message.setDescription(rs.getString("M.description"));
                user.setUserId(rs.getInt("fk_sender"));
                userMessage.setSender(user);
                user = new User();
                user.setUserId(rs.getInt("fk_recipient"));
                userMessage.setRecipient(user);
                userMessage.setDate(rs.getDate("UM.date_message"));
                userMessage.setMessage(message);
                listUserMessage.add(userMessage);

                userMessage = new UserMessage();
                message = new Message();
                user = new User();

                test = true;
            }

            if (!test) {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }

        return listUserMessage;
    }

    public List<UserMessage> selectSentMessagesByUserId(int userId) {

        this.sql = "SELECT M.title, M.description, UM.user_message_id, UM.fk_recipient, UM.date_message FROM USER_MESSAGE AS UM INNER JOIN MESSAGE AS M ON UM.fk_message = M.message_id WHERE UM.fk_sender =?";

        List<UserMessage> listUserMessage = new ArrayList<>();
        UserMessage userMessage = new UserMessage();
        Message message = new Message();
        User user = new User();

        try {

            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();
            boolean test = false;

            while (rs.next()) {

                userMessage.setUserMessageId(rs.getInt("UM.user_message_id"));
                message.setTitle(rs.getString("M.title"));
                message.setDescription(rs.getString("M.description"));
                user.setUserId(rs.getInt("UM.fk_recipient"));
                userMessage.setRecipient(user);
                userMessage.setDate(rs.getDate("UM.date_message"));
                userMessage.setMessage(message);
                listUserMessage.add(userMessage);
                userMessage = new UserMessage();
                message = new Message();
                user = new User();

                test = true;
            }

            if (!test) {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }

        return listUserMessage;
    }

    public List<UserMessage> selectReceivedMessagesByUserId(int userId) {

        this.sql = "SELECT M.title, M.description, UM.user_message_id, UM.fk_sender, UM.date_message FROM USER_MESSAGE AS UM INNER JOIN MESSAGE AS M ON UM.fk_message = M.message_id WHERE UM.fk_recipient =?";

        List<UserMessage> listUserMessage = new ArrayList<>();
        UserMessage userMessage = new UserMessage();
        Message message = new Message();
        User user = new User();

        try {

            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();
            boolean test = false;

            while (rs.next()) {

                userMessage.setUserMessageId(rs.getInt("UM.user_message_id"));
                message.setTitle(rs.getString("M.title"));
                message.setDescription(rs.getString("M.description"));
                user.setUserId(rs.getInt("UM.fk_sender"));
                userMessage.setSender(user);
                userMessage.setDate(rs.getDate("UM.date_message"));
                userMessage.setMessage(message);
                listUserMessage.add(userMessage);
                userMessage = new UserMessage();
                message = new Message();
                user = new User();

                test = true;
            }

            if (!test) {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }

        return listUserMessage;
    }
}
