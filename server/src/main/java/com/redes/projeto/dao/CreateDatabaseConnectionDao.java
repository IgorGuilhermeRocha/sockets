package com.redes.projeto.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.redes.projeto.utils.Utils;

public class CreateDatabaseConnectionDao {

    private static Connection conn = null;

    // Create and return connection with database
    public static Connection CreateDatabaseConnection() throws SQLException {
        if (conn == null || conn.isClosed() == true) {
            try {
                Properties properties = Utils.loadProperties("src/main/java/com/redes/projeto/db/db.properties");
                String user = properties.getProperty("user");
                String password = properties.getProperty("password");
                String dburl = properties.getProperty("dburl");
                conn = DriverManager.getConnection(dburl, user, password);

            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Connection database error");
                return null;
            }

        }
        return conn;

    }

}
