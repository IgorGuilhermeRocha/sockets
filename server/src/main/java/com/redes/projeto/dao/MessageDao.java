package com.redes.projeto.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageDao {

    private Connection conn;

    private String sql;

    public Integer insertMessage(String title, String description) {
        this.sql = "INSERT INTO MESSAGE VALUES (default,?, ?)";

        try {
            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql, java.sql.Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, title);
            ps.setNString(2, description);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }
        return null;
    }
}
