package com.redes.projeto.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.redes.projeto.entities.User;

public class UserDao {

    private String sql;
    private Connection conn;

    public User selectUserById(Integer userId) {
        this.sql = "SELECT * FROM USER WHERE user_id = ?";
        User user = new User();

        try {

            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                user.setEmail(rs.getString("email"));
                user.setName(rs.getString("name"));
                user.setLastName(rs.getString("last_name"));
                return user;
            }

        } catch (Exception e) {
            System.out.println("ERROR");
        }

        return user;
    }

    public Integer selectUserIdByEmail(String email) {

        this.sql = "SELECT user_id FROM USER WHERE email = ?";

        try {
            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("user_id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }
        return null;
    }

    public Integer insertUser(User user) {

        this.sql = "INSERT INTO USER VALUES (default, ? , ? , ? , ?)";

        try {
            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, user.getName());
            ps.setString(2, user.getLastName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            int result = ps.executeUpdate();
            return result;

        } catch (SQLException e) {
            System.out.println("Connection database error.");
            return -1;

        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }

    }

    public User selectUserByEmailAndPassword(String email, String password) {

        this.sql = "SELECT * FROM USER WHERE email=? AND password=?";

        User user = null;

        try {

            this.conn = CreateDatabaseConnectionDao.CreateDatabaseConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, email);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            user = new User();

            while (rs.next()) {
                user.setUserId(rs.getInt("user_id"));
                user.setName(rs.getString("name"));
                user.setLastName(rs.getString("last_name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection database error.");
            return null;
        } finally {
            try {
                this.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Close connection error");
            }
        }

        return null;
    }

}
