package com.redes.projeto.services;

import com.redes.projeto.dao.MessageDao;

public class MessageService {

    private MessageDao messageDao = new MessageDao();

    public Integer insertMessage(String title, String description) {
        return messageDao.insertMessage(title, description);
    }

}
