package com.redes.projeto.services;

import java.util.List;

import com.redes.projeto.dao.UserMessageDao;
import com.redes.projeto.entities.UserMessage;

public class UserMessageService {

    private UserMessageDao userMessageDao = new UserMessageDao();

    public List<UserMessage> getAllMessagesByUserId(int userId) {
        return userMessageDao.selectAllMessagesByUserId(userId);
    }

    public List<UserMessage> getAllSentMessages(int userId) {
        return userMessageDao.selectSentMessagesByUserId(userId);
    }

    public List<UserMessage> getAllReceivedMessages(int userId) {
        return userMessageDao.selectReceivedMessagesByUserId(userId);
    }

    public void insertUserMessage(int messageId, int senderId, int recipientId) {
        userMessageDao.InsertUserMessage(messageId, senderId, recipientId);
    }

}
