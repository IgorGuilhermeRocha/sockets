package com.redes.projeto.services;

import com.redes.projeto.dao.UserDao;
import com.redes.projeto.entities.User;

public class UserService {

    private UserDao userDao = new UserDao();

    public User login(String email, String password) {
        return userDao.selectUserByEmailAndPassword(email, password);
    }

    public Integer insertUser(User user) {
        return userDao.insertUser(user);

    }

    public Integer selectUserByEmail(String email) {
        return userDao.selectUserIdByEmail(email);
    }

    public User selectUserById(Integer userId) {
        return userDao.selectUserById(userId);
    }

}
