package com.redes.projeto.entities;

import java.io.Serializable;

public class User implements Serializable {

    // attrs
    private Integer userId;

    private String name;

    private String lastName;

    private String email;

    private String password;

    // constructors
    public User() {

    }

    public User(String name, String lastName, String email, String password) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public User(Integer userId, String name, String lastName, String email, String password) {

        this.userId = userId;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;

    }

    // getters and setters

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // to String
    @Override
    public String toString() {
        return "User [email=" + email + ", userId=" + userId + ", lastName=" + lastName + ", name=" + name
                + ", password=" + password + "]";
    }

}
