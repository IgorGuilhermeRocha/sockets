package com.redes.projeto.entities;

import java.io.Serializable;
import java.sql.Date;

public class UserMessage implements Serializable {

    // attrs

    private Integer userMessageId;

    private User sender;

    private User recipient;

    private Message message;

    private Date date;

    // contructors

    public UserMessage() {

    }

    // getters and setters

    public Integer getUserMessageId() {
        return userMessageId;
    }

    public void setUserMessageId(Integer userMessageId) {
        this.userMessageId = userMessageId;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    // to String
    @Override
    public String toString() {
        return "UserMessage [date=" + date + ", message=" + message + ", recipient=" + recipient + ", sender=" + sender
                + ", userMessageId=" + userMessageId + "]";
    }

}
