package com.redes.projeto.entities;

import java.io.Serializable;

public class Message implements Serializable {

    // attr

    private Integer messageId;
    private String title;
    private String description;

    // contructors

    public Message() {

    }

    public Message(String title, String description) {
        this.title = title;
        this.description = description;
    }

    // getters and setters

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // to String
    @Override
    public String toString() {
        return title;
    }

}
