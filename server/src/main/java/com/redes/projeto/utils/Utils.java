package com.redes.projeto.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utils {

    public static Properties loadProperties(String path) throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream(path);
        props.load(file);
        return props;

    }

}
