package com.redes.projeto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import com.redes.projeto.entities.Request;
import com.redes.projeto.entities.User;
import com.redes.projeto.entities.UserMessage;
import com.redes.projeto.services.MessageService;
import com.redes.projeto.services.UserMessageService;
import com.redes.projeto.services.UserService;

public class Protocol implements Runnable {
    private UserService userService;
    private MessageService messageService;
    private UserMessageService userMessageService;
    private Socket clientSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    public Protocol(Socket clientSocket) throws Exception, SQLIntegrityConstraintViolationException {
        super();
        this.clientSocket = clientSocket;
        this.out = new ObjectOutputStream(this.clientSocket.getOutputStream());
        this.in = new ObjectInputStream(this.clientSocket.getInputStream());

    }

    public void sendMessage(Object message) throws Exception {
        this.out.writeObject(message);
    }

    public Object receiveMessage() throws Exception {
        return this.in.readObject();
    }

    public void closeSocket() throws IOException {
        this.out.close();
        this.in.close();
        this.clientSocket.close();
    }

    @Override
    public void run() {
        try {
            Request clientRequest = (Request) this.receiveMessage();
            this.whichOperation(clientRequest);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Connection ERROR");
        }
    }

    private void whichOperation(Request clientRequest) throws Exception {

        switch (clientRequest.getOperationNum()) {
            case 1:
                this.loginOperation(clientRequest);
                break;
            case 2:
                this.creatNewUser();
                break;
            case 3:
                this.getListOfMessages(clientRequest);
                break;
            case 4:
                this.sendMessage(clientRequest);
                break;
            case 5:
                this.getUser();
                break;

        }
    }

    private void loginOperation(Request clientRequest) throws Exception {
        userService = new UserService();
        User user = userService.login(clientRequest.getInfo().get(0), clientRequest.getInfo().get(1));
        this.sendMessage(user);
        this.closeSocket();
    }

    private void creatNewUser() throws Exception, SQLIntegrityConstraintViolationException {
        userService = new UserService();
        User user;
        user = (User) this.receiveMessage();
        // if result > 1 the user has been created
        Integer result = this.userService.insertUser(user);
        this.sendMessage(result);
        this.closeSocket();

    }

    private void getListOfMessages(Request clientRequest) throws Exception {
        userMessageService = new UserMessageService();
        Object aux;
        aux = this.receiveMessage();
        int userId = (int) aux;
        String info;
        List<UserMessage> listUserMessage = null;

        if (clientRequest.getInfo() != null) {
            info = clientRequest.getInfo().get(0);
            if (info.equals("1")) {
                listUserMessage = this.userMessageService.getAllSentMessages(userId);
            } else {
                listUserMessage = this.userMessageService.getAllReceivedMessages(userId);
            }
        } else {
            listUserMessage = this.userMessageService.getAllMessagesByUserId(userId);
        }

        this.sendMessage(listUserMessage);
        this.closeSocket();

    }

    private void sendMessage(Request clientRequest) throws Exception {
        userService = new UserService();
        messageService = new MessageService();
        userMessageService = new UserMessageService();

        User sender = (User) this.receiveMessage();
        Integer recipientId = userService.selectUserByEmail(clientRequest.getInfo().get(0));

        if (recipientId != null) {
            this.sendMessage(1);
            int messageId = messageService.insertMessage(clientRequest.getMesssage().getTitle(),
                    clientRequest.getMesssage().getDescription());
            userMessageService.insertUserMessage(messageId, sender.getUserId(),
                    recipientId);
        } else {
            this.sendMessage(0);
        }

        this.closeSocket();
    }

    private void getUser() throws Exception {
        this.userService = new UserService();
        Integer userId = (Integer) this.receiveMessage();
        User user = this.userService.selectUserById(userId);
        this.sendMessage(user);
        this.closeSocket();
    }

}
