package com.redes.projeto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.redes.projeto.dao.CreateServerConnectionDao;
import com.redes.projeto.entities.Request;
import com.redes.projeto.entities.User;
import com.redes.projeto.utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.Node;

public class LoginViewController {

    // attrs

    private CreateServerConnectionDao serverConnection;
    private Request request;

    // FXML components

    // Labels

    @FXML
    private Label lbEmailError;

    @FXML
    private Label lbPasswordError;

    // TextFields and PasswordField

    @FXML
    private TextField tfEmail;

    @FXML
    private PasswordField pfPassword;

    // Buttons

    @FXML
    private Button btLogin;

    @FXML
    private Button btCreateAccount;

    // FXML methods

    @FXML
    private void ActionEventBtLogin(ActionEvent e) {
        this.restartLabels();
        boolean validPassword = Utils.passwordIsEmpty(pfPassword, lbPasswordError);

        if (!this.verifyTextIsEmpty() && !validPassword) {
            try {

                List<String> info = new ArrayList<>();
                info.add(this.getTfEmail());
                info.add(this.getPfPassword());
                request = new Request(info, 1);
                this.serverConnection = Utils.startConnectionWithServer(this.serverConnection);
                this.serverConnection.sendMessage(request);
                User user = (User) this.serverConnection.receiveMessage();
                this.serverConnection.closeSocket();

                if (user != null) {
                    Utils.closeWindow(e);
                    MainViewController.setLoggedIn(user);
                    Utils.createNewWindow(new Stage(), "mainView", "JIMAIL");
                } else {
                    Utils.errorWindow("ERROR", "Login error", "Wrong email or password");
                }

            } catch (Exception e1) {
                e1.printStackTrace();
                Utils.errorWindow("ERROR", "Server error", "Cant connect with server");
            }
        }
    }

    @FXML
    private void ActionEventBtcreateNewAccount(ActionEvent e) {
        Stage myStage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        try {
            Utils.createNewWindow(new Stage(), myStage,
                    "createAccountView", "Create account");
        } catch (IOException e1) {
            e1.printStackTrace();
            System.out.println("CREATE WINDOW ERROR");
        }
    }

    // General methods

    private boolean verifyTextIsEmpty() {
        List<TextField> listTf = new ArrayList<>();
        listTf.add(this.tfEmail);
        List<Label> lLabel = new ArrayList<>();
        lLabel.add(this.lbEmailError);
        return Utils.textFieldsIsEmpty(listTf, lLabel) && (!Utils.passwordIsEmpty(pfPassword, lbPasswordError));

    }

    private void restartLabels() {
        this.lbEmailError.setVisible(false);
        this.lbPasswordError.setVisible(false);
        this.lbPasswordError.setVisible(false);
    }

    // Getters and Setters

    public String getTfEmail() {
        return this.tfEmail.getText();
    }

    public void setTfEmail(String text) {
        this.tfEmail.setText(text);
    }

    public String getPfPassword() {
        return this.pfPassword.getText();
    }

    public void setPfPassword(String text) {
        this.pfPassword.setText(text);
    }

}
