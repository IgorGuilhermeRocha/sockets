package com.redes.projeto;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import com.redes.projeto.dao.CreateServerConnectionDao;
import com.redes.projeto.entities.Request;
import com.redes.projeto.entities.User;
import com.redes.projeto.utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class CreateAccountController {

    // attributes

    // General attrs
    private CreateServerConnectionDao serverConnection;

    // JavaFX attrs

    // TextFields

    @FXML
    private TextField tfName;

    @FXML
    private TextField tfLastName;

    @FXML
    private TextField tfEmail;

    @FXML
    private PasswordField pfPassword;

    // Labels
    @FXML
    private Label lbNameError;

    @FXML
    private Label lbLastNameError;

    @FXML
    private Label lbEmailError;

    @FXML
    private Label lbPasswordError;

    // Buttons

    @FXML
    private Button btConfirm;

    @FXML
    private Button btCancel;

    // methods

    // FXML methods

    @FXML
    private void actionEventBtConfirm(ActionEvent event) {
        restartLabels();
        boolean validPassword = Utils.passwordIsEmpty(pfPassword, lbPasswordError);

        if (!verifyTextIsEmpty() && !validPassword) {

            try {
                Request request = new Request(2);
                User user = new User(tfName.getText(), tfLastName.getText(), tfEmail.getText(), pfPassword.getText());
                this.serverConnection = Utils.startConnectionWithServer(this.serverConnection);
                this.serverConnection.sendMessage(request);
                this.serverConnection.sendMessage(user);
                Object t = this.serverConnection.receiveMessage();
                this.serverConnection.closeSocket();
                Integer result = (Integer) t;

                if (result == -1) {
                    Utils.errorWindow("ERROR", "Create user error", "this email already in use");
                } else {
                    Utils.closeWindow(event);
                }

            } catch (SQLIntegrityConstraintViolationException sql) {

            } catch (Exception e) {
                Utils.errorWindow("ERROR", "Server error", "Cant connect with server");
            }
        }

    }

    @FXML
    private void actioEventBtCancel(ActionEvent e) {
        Utils.requestCloseWindow(e, "Close window", "", "Are you sure ?");
    }

    // General methods

    private void restartLabels() {
        this.lbNameError.setVisible(false);
        this.lbLastNameError.setVisible(false);
        this.lbEmailError.setVisible(false);
        this.lbPasswordError.setVisible(false);
    }

    private boolean verifyTextIsEmpty() {
        List<TextField> listTf = new ArrayList<>();
        listTf.add(tfName);
        listTf.add(tfLastName);
        listTf.add(this.tfEmail);
        List<Label> lLabel = new ArrayList<>();
        lLabel.add(this.lbNameError);
        lLabel.add(this.lbLastNameError);
        lLabel.add(this.lbEmailError);
        lLabel.add(this.lbPasswordError);
        return Utils.textFieldsIsEmpty(listTf, lLabel);

    }
}
