package com.redes.projeto;

import java.util.ArrayList;
import java.util.List;

import com.redes.projeto.dao.CreateServerConnectionDao;
import com.redes.projeto.entities.Message;
import com.redes.projeto.entities.Request;
import com.redes.projeto.entities.User;
import com.redes.projeto.utils.Utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class SendMessageController {

    // attrs

    private static User user;

    private CreateServerConnectionDao serverConnection;

    // FXML components

    // TextFields,Area

    @FXML
    private TextField tfRecipientEmail;

    @FXML
    private TextField tfMessageTitle;

    @FXML
    private TextArea taMessageDescription;

    // Labels

    @FXML
    private Label lbRecipientEmailError;

    @FXML
    private Label lbTitleError;

    @FXML
    private Label lbDescriptionError;

    // Buttons

    @FXML
    private Button btSend;

    @FXML
    private Button btCancel;

    // FXML methods

    @FXML
    private void actionEventBtSend(ActionEvent event) {

        // set labels visible to false
        List<Label> listOfLabels = new ArrayList();
        listOfLabels = this.getAllLabels();
        listOfLabels.add(this.lbDescriptionError);

        Utils.restartLabels(listOfLabels);

        // Verify if email or title is null
        boolean textFieldsIsNull = Utils.textFieldsIsEmpty(this.getAllTextFields(), this.getAllLabels());

        if ((!this.isDescriptionNull()) && (!textFieldsIsNull)) {

            if (this.isEqualsToMyEmail()) {
                Utils.errorWindow("ERROR", "", "Recipient emails is equals to yours.");
            } else {
                try {
                    List<String> info = new ArrayList<>();
                    info.add(tfRecipientEmail.getText());
                    this.serverConnection = Utils.startConnectionWithServer(this.serverConnection);
                    this.serverConnection.sendMessage(new Request(4, info,
                            new Message(tfMessageTitle.getText(), taMessageDescription.getText())));
                    this.serverConnection.sendMessage(user);
                    Object aux = this.serverConnection.receiveMessage();
                    this.serverConnection.closeSocket();
                    Integer result = (Integer) aux;

                    if (result == 0) {
                        Utils.errorWindow("ERROR", "", "This email does not exist");
                    } else {
                        Utils.closeWindow(event);
                    }
                } catch (Exception e) {
                    Utils.errorWindow("ERROR", "Server error", "Cant connect with server");
                }
            }

        }

    }

    @FXML
    private void actionEventBtCancel(ActionEvent event) {
        ButtonType bt = Utils.requestCloseWindow("Close Window", "", "Are you sure ?");
        if (bt == ButtonType.OK) {
            Utils.closeWindow(event);
        }
    }

    // General methods

    private Boolean isDescriptionNull() {
        if (taMessageDescription.getText().isEmpty() || taMessageDescription.getText().isBlank()) {
            lbDescriptionError.setVisible(true);
            return true;
        }
        return false;
    }

    private Boolean isEqualsToMyEmail() {
        if (user.getEmail().equals(this.tfRecipientEmail.getText())) {
            return true;
        }
        return false;
    }

    public List<TextField> getAllTextFields() {

        // return a list with all textField

        List<TextField> list = new ArrayList<>();
        list.add(this.tfRecipientEmail);
        list.add(this.tfMessageTitle);
        return list;
    }

    public List<Label> getAllLabels() {

        // return a list with all labels

        List<Label> list = new ArrayList<>();
        list.add(this.lbRecipientEmailError);
        list.add(this.lbTitleError);
        return list;
    }

    // getters and setters

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        SendMessageController.user = user;
    }
}
