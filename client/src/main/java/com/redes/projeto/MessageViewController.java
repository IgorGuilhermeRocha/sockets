package com.redes.projeto;

import java.net.URL;
import java.util.ResourceBundle;

import com.redes.projeto.dao.CreateServerConnectionDao;
import com.redes.projeto.entities.Request;
import com.redes.projeto.entities.User;
import com.redes.projeto.entities.UserMessage;
import com.redes.projeto.utils.Utils;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class MessageViewController implements Initializable {

    // attrs

    private CreateServerConnectionDao serverConnection;
    private static User loggIn;
    private static UserMessage userMessage;

    // FXML components

    // TextFields
    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfTitle;

    @FXML
    private TextField tfDate;

    // TextArea
    @FXML
    private TextArea taDescription;

    // methods

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            this.loadContent();
        } catch (Exception e) {
            Utils.errorWindow("ERROR", "", "Cant load message");
        }
    }

    private void loadContent() throws NumberFormatException, Exception {
        Integer userId = this.getUserId();
        User user = this.getUser(userId);
        this.setFields(user);
    }

    private void setFields(User user) throws NumberFormatException, Exception {

        this.tfTitle.setText(userMessage.getMessage().getTitle());
        this.taDescription.setText(userMessage.getMessage().getDescription());
        this.tfDate.setText(userMessage.getDate().toString());
        this.tfEmail.setText(user.getEmail());
    }

    private User getUser(Integer userId) throws NumberFormatException, Exception {

        this.serverConnection = Utils.startConnectionWithServer(this.serverConnection);
        this.serverConnection.sendMessage(new Request(5));
        this.serverConnection.sendMessage(userId);
        User user = (User) this.serverConnection.receiveMessage();
        this.serverConnection.closeSocket();

        return user;

    }

    private Integer getUserId() {

        Integer userId = 0;

        if (userMessage.getSender() != null) {
            if (!userMessage.getSender().getUserId().equals(loggIn.getUserId())) {
                userId = userMessage.getSender().getUserId();
            }
        }

        if (userId == 0) {
            userId = userMessage.getRecipient().getUserId();
        }

        return userId;
    }

    // getters and setters

    public static User getLoggIn() {
        return loggIn;
    }

    public static void setLoggIn(User loggIn) {
        MessageViewController.loggIn = loggIn;
    }

    public static UserMessage getUserMessage() {
        return userMessage;
    }

    public static void setUserMessage(UserMessage userMessage) {
        MessageViewController.userMessage = userMessage;
    }

}
