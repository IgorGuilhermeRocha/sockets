package com.redes.projeto.entities;

import java.io.Serializable;
import java.util.List;

public class Request implements Serializable {

    // attrs

    private Integer operationNum;

    private List<String> info;

    private Message messsage;

    // contructors

    public Request() {

    }

    public Request(int operationNum) {
        this.operationNum = operationNum;
    }

    public Request(List<String> info, int operationNum) {
        this.operationNum = operationNum;
        this.info = info;
    }

    public Request(Integer operationNum, List<String> info, Message messsage) {
        this.operationNum = operationNum;
        this.info = info;
        this.messsage = messsage;
    }

    // getters and setters

    public Integer getOperationNum() {
        return operationNum;
    }

    public void setOperationNum(Integer operationNum) {
        this.operationNum = operationNum;
    }

    public List<String> getInfo() {
        return info;
    }

    public void setInfo(List<String> info) {
        this.info = info;
    }

    public Message getMesssage() {
        return messsage;
    }

    public void setMesssage(Message messsage) {
        this.messsage = messsage;
    }

    // to string
    @Override
    public String toString() {
        return "Request [info=" + info + ", messsage=" + messsage + ", operationNum=" + operationNum + "]";
    }

}