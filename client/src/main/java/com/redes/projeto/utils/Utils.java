package com.redes.projeto.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import com.redes.projeto.App;
import com.redes.projeto.dao.CreateServerConnectionDao;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Utils {

    // Create windows
    public static void createNewWindow(Stage stage, String viewName, String title) throws IOException {
        Scene scene = new Scene(loadFXML(viewName));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle(title);
        stage.show();
    }

    public static void createNewWindow(Stage stage, Stage stageOwner, String viewName, String title)
            throws IOException {
        Scene scene = new Scene(loadFXML(viewName));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(stageOwner);
        stage.setTitle(title);
        stage.show();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    // start Connection with server

    public static CreateServerConnectionDao startConnectionWithServer(CreateServerConnectionDao conn)
            throws NumberFormatException, Exception {

        Properties properties = loadProperties("src/main/java/com/redes/projeto/server/server.properties");
        String ip = properties.getProperty("ip");
        String port = properties.getProperty("port");
        conn = new CreateServerConnectionDao(ip, Integer.parseInt(port));
        return conn;
    }

    // load properties

    public static Properties loadProperties(String path) throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream(path);
        props.load(file);
        return props;

    }

    // create alerts and close windows

    public static void closeWindow(ActionEvent e) {
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.close();
    }

    public static ButtonType requestCloseWindow(String title, String headerText, String bodyText) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(bodyText);
        return alert.showAndWait().get();

    }

    public static void requestCloseWindow(ActionEvent e, String title, String headerText, String bodyText) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(bodyText);

        if (alert.showAndWait().get() == ButtonType.OK) {
            Utils.closeWindow(e);
        }
    }

    public static void errorWindow(String title, String headerText, String bodyText) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(bodyText);
        alert.show();
    }

    public static void restartLabels(List<Label> list) {
        for (Label label : list) {
            label.setVisible(false);
        }
    }

    // Verify empty inputs

    public static boolean textFieldsIsEmpty(List<TextField> listTf, List<Label> lLabel) {
        boolean test = false;
        for (int i = 0; i < listTf.size(); i++) {
            if (listTf.get(i).getText().isEmpty() || listTf.get(i).getText().replaceAll(" ", "").equals("")) {
                lLabel.get(i).setVisible(true);
                test = true;
            }
        }
        return test;
    }

    public static boolean passwordIsEmpty(PasswordField ps, Label error) {
        if (ps.getText().isEmpty() || ps.getText().replaceAll(" ", "").equals("")) {
            error.setVisible(true);
            return true;
        }
        return false;
    }
}
