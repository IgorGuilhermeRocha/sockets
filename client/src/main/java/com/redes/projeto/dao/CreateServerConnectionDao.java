package com.redes.projeto.dao;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class CreateServerConnectionDao {

    private Socket clientSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    public CreateServerConnectionDao(String address, int port) throws Exception {
        super();
        this.clientSocket = new Socket(address, port);
        this.out = new ObjectOutputStream(this.clientSocket.getOutputStream());
        this.in = new ObjectInputStream(this.clientSocket.getInputStream());
    }

    public void sendString(String message) throws Exception {
        this.out.writeUTF(message);
    }

    public String receiveString() throws Exception {
        return this.in.readUTF();
    }

    public void sendMessage(Object message) throws Exception {
        this.out.writeObject(message);
    }

    public Object receiveMessage() throws Exception {
        return this.in.readObject();
    }

    public void closeSocket() throws IOException {
        this.out.close();
        this.in.close();
        this.clientSocket.close();
    }

}
