package com.redes.projeto;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import com.redes.projeto.dao.CreateServerConnectionDao;
import com.redes.projeto.entities.Message;
import com.redes.projeto.entities.Request;
import com.redes.projeto.entities.User;
import com.redes.projeto.entities.UserMessage;
import com.redes.projeto.utils.Utils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.Node;

public class MainViewController implements Initializable {

    // attrs

    private static User loggedIn;

    private ObservableList obsList;

    private CreateServerConnectionDao serverConnection;

    // FXML components

    // Table View
    @FXML
    private TableView<UserMessage> tvMessages;

    // Table columns
    @FXML
    private TableColumn<UserMessage, Integer> tcIdUserMessage;

    @FXML
    private TableColumn<Message, Message> tcTitle;

    @FXML
    private TableColumn<UserMessage, Date> tcDate;

    // Buttons

    @FXML
    private Button btLogoff;

    @FXML
    private Button btSendMail;

    // Labels
    @FXML
    private Label lbName;

    @FXML
    private Label lbLastName;

    // FXML methods
    @FXML
    private void actionEventBtLogOff(ActionEvent event) {
        ButtonType bt = Utils.requestCloseWindow("Close Window", "", "Are you sure ?");
        if (bt == ButtonType.OK) {
            Utils.closeWindow(event);
            try {
                Utils.createNewWindow(new Stage(), "loginView", "LOGIN");
                Utils.closeWindow(event);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("ERROR CREATE LOGIN VIEW");
            }
        }
    }

    @FXML
    private void actionEventBtSendEmail(ActionEvent event) {
        try {
            Utils.createNewWindow(new Stage(), (Stage) ((Node) event.getSource()).getScene().getWindow(),
                    "sendMailView",
                    "SEND MAIL");
            SendMessageController.setUser(this.loggedIn);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Create window error");
        }
    }

    @FXML
    private void actionEventGetSentMessages(ActionEvent actionEvent) {
        this.setTable("1");
    }

    @FXML
    private void actionEventGetReceivedMessages(ActionEvent actionEvent) {
        this.setTable("2");
    }

    @FXML
    private void actionEventGetAllMessages(ActionEvent actionEvent) {
        this.setTable();

    }

    @FXML
    private void mouseEventViewMessage(MouseEvent mouseEvent) throws IOException {
        UserMessage userMessage = tvMessages.getSelectionModel().getSelectedItem();

        if (userMessage != null) {
            MessageViewController.setUserMessage(userMessage);
            MessageViewController.setLoggIn(this.loggedIn);
            Utils.createNewWindow(new Stage(), (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow(),
                    "messageView", "Message");
        }

    }

    // General methods

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initNodes();
        this.setTable();
    }

    private void setTable() {
        Request request = new Request(3);
        this.loadItens(request);

    }

    private void setTable(String op) {

        List<String> list = new ArrayList<>();
        list.add(op);

        Request request = new Request(list, 3);
        this.loadItens(request);

    }

    private void initNodes() {
        this.lbName.setText(loggedIn.getName());
        this.lbLastName.setText(loggedIn.getLastName());
        this.tcIdUserMessage.setCellValueFactory(new PropertyValueFactory<>("userMessageId"));
        this.tcTitle.setCellValueFactory(new PropertyValueFactory<>("message"));
        this.tcDate.setCellValueFactory(new PropertyValueFactory<>("date"));
    }

    private void loadItens(Request request) {
        List<UserMessage> list;
        boolean isNull = false;

        try {

            this.serverConnection = Utils.startConnectionWithServer(this.serverConnection);
            this.serverConnection.sendMessage(request);
            this.serverConnection.sendMessage(loggedIn.getUserId());
            list = (List<UserMessage>) this.serverConnection.receiveMessage();

            if (list == null) {
                list = new ArrayList<>();

            }

            this.obsList = FXCollections.observableArrayList(list);
            this.loadTable();

        } catch (Exception e) {
            Utils.errorWindow("ERROR", "Server error", "Cant connect with server");
        } finally {
            try {
                this.serverConnection.closeSocket();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private void loadTable() {
        tvMessages.setItems(this.obsList);
    }

    // getters and setters

    public static User getLoggedIn() {
        return loggedIn;
    }

    public static void setLoggedIn(User loggedIn) {
        MainViewController.loggedIn = loggedIn;
    }

}
