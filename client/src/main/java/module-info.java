module com.redes.projeto {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens com.redes.projeto to javafx.fxml;
    opens com.redes.projeto.entities to javafx.base;

    exports com.redes.projeto;
}
