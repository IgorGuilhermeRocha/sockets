# Sockets

## Sobre o projeto.

> Este é um projeto que fiz para a disciplina de redes, do curso de Sistemas de Informação, ele busca simular o funcionamento básico de um email, ou seja, busca fazer operações de login/logout, envio/recebimento e filtragem de mensagens.

---

## Estrutura do projeto.

>O projeto é dividido em duas partes: 
- A parte do cliente, que esta na pasta client.
- A parte do servidor, que esta na pasta server.

>A aplicação cliente é responsável por fazer requisições ao servidor, já o servidor trata estas requisições e busca/insere as informações necessárias no banco de dados. Tudo isso por meio de **sockets**.

---

## Principais tecnologias utilizadas:

- Java 11
- Maven 3.8.5
- JavaFx 13
- Mysql

---

## Como rodar o projeto.
 1. Primeiro passo:
 
 Na raiz do projeto existe uma pasta com o nome de **database-script**, nela você terá acesso ao script do banco.

 ---
 ---

 2. Segundo passo:

 Com o script em mãos, você podera criar um banco semelhante ao meu, no seu desktop, importando o mesmo com o uso do workbench ou até mesmo via linha de comando. Caso a importação dê errado você também poderá abrir esse aquivo em forma de texto e copiar os scripts.

 ---
 ---

 3. Terceiro passo:

Mude o arquivo db.properties que esta em: server > src > main > java > com > redes > projeto > db > db.properties.

Altere as informações desse arquivo com base no seu banco local.

Exemplo:

user=[seu usuario]  
password=[sua senha]  
dburl=jdbc:mysql://localhost:[porta]/JIMAIL  
userSSL=false  

A porta provavelmente vai ser a padrão mesmo, no caso a 3306. O user vai ser seu usuário no banco e password sua senha. Caso você queira continuar como root pode apenas trocar a senha.

---
---

4. Quarto passo:


<html>
    <p>
    <font color=red>Warning:</font>
    Tenha em mente que você deve possuir o jdk e o maven instalados na sua máquina.  
    </p>
</html>

Com o banco configurado e rodando. Vamos iniciar o server.

Para iniciar o server 


```bash

## Em uma IDE de sua preferência abra a pasta server, que esta na raiz do projeto.

## Apos isto, execute a classe App.java.

## Se não ocorreu nenhum erro volte para a pasta raiz do projeto.

## Agora abra o terminal na pasta : client > target .

## Execute este comando para iniciar a aplicação cliente.

$java -jar [nome_do_arquivo]

## substitua [nome_do_arquivo] pelo nome do jar.

```

---

### Observações:

>Caso você tenha uma versão muito antiga do jdk talvez a aplicação client não vai funcionar, é melhor que seja pelo menos a 9 pra cima.

---

### Vídeo da aplicação rodando.




### Como me achar.

>Se você caiu aqui de paraquedas e quer conversar comigo me manda um email aqui:
igorguilhermedev@gmail.com.




